<?php
/**
 * @file
 * manage_your_department_content.features.inc
 */

/**
 * Implements hook_views_api().
 */
function manage_your_department_content_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
